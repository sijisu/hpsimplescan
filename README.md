# HPSimpleScan

## Download

[Download for Linux](https://gitlab.com/sijisu/hpsimplescan/-/jobs/artifacts/master/raw/hpsimplescan?job=compile)

[Download for Windows](https://gitlab.com/sijisu/hpsimplescan/-/jobs/artifacts/master/raw/hpsimplescan.exe?job=compile)


## Usage

```
NAME:
   HPSimpleScan - simple scanning for some older HP printers/scanners, especially the HP LaserJet 100 colorMFP M175nw

USAGE:
   hpsimplescan [global options] command [command options] [arguments...]

VERSION:
   v0.2

COMMANDS:
   status, i  get the current scanner/printer status
   scan, s    scan from the scanner to file
   help, h    Shows a list of commands or help for one command

GLOBAL OPTIONS:
   -i IP          IP or hostname of the scanner/printer to connect to (default: "192.168.1.3")
   -p port        port of the SOAP API on scanner/printer (default: 8289)
   --debug, -d    debug output (default: false)
   --verbose, -v  verbose output (default: false)
   --help, -h     show help (default: false)
   --version, -V  print the version (default: false)

```
## Example

### Get scanner status

```
$ hpsimplescan status
```

### Scan

```
$ hpsimplescan scan
```

### Scan to specified file

```
$ hpsimplescan scan -f ./scan.jpg
```