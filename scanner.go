package main

import (
	"bytes"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/google/uuid"
	"github.com/kaitai-io/kaitai_struct_go_runtime/kaitai"
)

/*
Scanner stores state about the scanner
*/
type Scanner struct {
	address string
	port    int
}

type scannerError struct {
	resp    *http.Response
	Subcode string
	msg     string
}

func (m *scannerError) Error() string {
	resp := *(m.resp)
	if resp.StatusCode >= 400 {
		errorResp := errorResponse{}

		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			goto fallback
		}

		err = xml.Unmarshal(bodyBytes, &errorResp)
		if err != nil {
			goto fallback
		}

		m.Subcode = errorResp.Body.Fault.Code.Subcode.Value

		s, _ := json.MarshalIndent(errorResp, "", "\t")
		return fmt.Sprintf("CODE: %d scanner error: the scanner returned: %s", resp.StatusCode, s)
	}

	return fmt.Sprintf("scanner error %s", m.msg)

fallback:
	return fmt.Sprintf("CODE: %d scanner error (and response non-standard) %s", resp.StatusCode, m.msg)
}

const (
	InputSourcePlaten = "Platen"
	InputSourceADF    = "ADF"
)

func (s Scanner) getStatus() (*getScannerElementsResponse, error) {
	log.Info("getting scanner status")
	requestStructure := getScannerElementsRequest{}
	requestStructure.prepare()
	responseStructure, err := s.makeXMLRequest(requestStructure, &getScannerElementsResponse{})
	if err != nil {
		log.Critical("error: ", err)
		return nil, &scannerError{}
	}

	return responseStructure.(*getScannerElementsResponse), nil
}

func (s Scanner) sendScanRequest(inputSource string) (*createScanJobRequestResponse, error) {
	log.Info("sending scanner job request")
	requestStructure := createScanJobRequestRequest{}
	requestStructure.prepare()

	jobID := uuid.New()

	requestStructure.Body.CreateScanJobRequest.ScanIdentifier.Text = jobID.String()
	requestStructure.Body.CreateScanJobRequest.ScanTicket.RetrieveImageTimeout.Text = 1800

	documentParams := &requestStructure.Body.CreateScanJobRequest.ScanTicket.DocumentParameters

	documentParams.Format.Text = "jfif"

	documentParams.Exposure.AutoExposure.Text = true
	documentParams.Exposure.ExposureSettings.Contrast.Text = 0
	documentParams.ContentType.Text = "Auto"
	documentParams.CompressionQualityFactor.Text = 0
	documentParams.InputSource.Text = inputSource
	documentParams.InputSize.InputMediaSize.Height.Text = 11690
	documentParams.InputSize.InputMediaSize.Width.Text = 8500
	documentParams.MediaSides.MediaFront.Resolution.Height.Text = 300
	documentParams.MediaSides.MediaFront.Resolution.Width.Text = 300
	documentParams.MediaSides.MediaFront.ScanRegion.ScanRegionXOffset.Text = 0
	documentParams.MediaSides.MediaFront.ScanRegion.ScanRegionYOffset.Text = 0
	documentParams.MediaSides.MediaFront.ScanRegion.ScanRegionHeight.Text = 11000
	// Full size
	//documentParams.MediaSides.MediaFront.ScanRegion.ScanRegionHeight.Text = 11690
	documentParams.MediaSides.MediaFront.ScanRegion.ScanRegionWidth.Text = 8500
	documentParams.MediaSides.MediaFront.ColorProcessing.Text = "RGB24"

	responseStructure, err := s.makeXMLRequest(requestStructure, &createScanJobRequestResponse{})
	if err != nil {
		log.Critical(err)
		return responseStructure.(*createScanJobRequestResponse), &scannerError{}
	}

	return responseStructure.(*createScanJobRequestResponse), nil
}

func (s Scanner) sendGetJobInfoRequest(jobID int) (*getJobInfoResponse, error) {
	log.Infof("getting job info for id %d", jobID)
	requestStructure := getJobInfoRequest{}
	requestStructure.prepare()

	requestStructure.Body.GetJobInfo.JobId.Text = jobID

	responseStructure, err := s.makeXMLRequest(requestStructure, &getJobInfoResponse{})
	if err != nil {
		log.Critical(err)
		return responseStructure.(*getJobInfoResponse), &scannerError{}
	}

	return responseStructure.(*getJobInfoResponse), nil
}

func (s Scanner) sendRetrieveImageRequest(jobID int, jobToken string) (*retrieveImageRequestResponse, error) {
	log.Infof("retrieving scan request with id %d and token %s", jobID, jobToken)
	requestStructure := retrieveImageRequestRequest{}
	requestStructure.prepare()

	requestStructure.Body.RetrieveImageRequest.JobId.Text = jobID
	requestStructure.Body.RetrieveImageRequest.JobToken.Text = jobToken

	responseStructure := retrieveImageRequestResponse{}

	output, err := xml.MarshalIndent(requestStructure, "", "")
	if err != nil {
		return nil, err
	}

	bodyBytes, err := s.makeRequest(output)
	if err != nil {
		return nil, err
	}

	dime := NewDimeMessage()
	stream := kaitai.NewStream(bytes.NewReader(bodyBytes))
	err = dime.Read(stream, dime, dime)
	if err != nil {
		return nil, err
	}

	//fmt.Printf("%+v", dime.Dime.Message)

	err = xml.Unmarshal(dime.Records[0].Data, &responseStructure.XML)
	if err != nil {
		return nil, err
	}

	for index, record := range dime.Records {
		if index == 0 {
			continue
		}
		responseStructure.Image = append(responseStructure.Image, record.Data...)
	}

	return &responseStructure, nil
}

func (s Scanner) writeImageToFile(name string, buffer []byte) error {
	log.Debug("Writing into ", name)
	err := ioutil.WriteFile(name, buffer, 0700)
	return err
}

func (s Scanner) prepareDirectory(name string) error {
	log.Debug("Preparing directory ", name)
	_, err := os.Stat(name)
	if os.IsNotExist(err) {
		log.Debug("Folder does not exist, creating...")
		err := os.Mkdir(name, 0700)
		return err
	}
	panic("folder already exists") // for now, error handling later
}

func (s Scanner) generateFileName(folder string) (filename string) {
	return fmt.Sprintf("%s/scan-%s.jpg", folder, time.Now().Format("20060102-150405"))
}

func (s Scanner) generateDirectoryName() (filename string) {
	return fmt.Sprintf("./scans-%s", time.Now().Format("20060102-150405"))
}

func (s Scanner) scan(filename string) error {
	if strings.Trim(filename, " ") == "" {
		log.Notice("filename not provided, generating")
		filename = s.generateFileName(".")
	}

	log.Info("Scanning into ", filename)
	/*
		TODOs in order by priority
		TODO: set options with flags
		TODO: support for ADF and multipage scanning
			should be pretty easy, just loop until the scanner is idle
			or even better, look how the app does it
		TODO: proper option managment → don't allow an option that is not supported by the scanner
			some nice struct and shared interface magic would be nice
		TODO: hpraw format parsing
			I don't know what the benefits would be, but it would be kinda cool
			it's raw RGB24(or according to settings) RGB pixel data, see https://rawpixels.net/
		TODO: probably others I can't remember right now
	*/
	r, err := s.sendScanRequest(InputSourcePlaten)
	if err != nil {
		return err
	}

	var id int = r.Body.CreateScanJobResponseType.JobId
	token := r.Body.CreateScanJobResponseType.JobToken
	time.Sleep(time.Second)
	_, err = s.sendGetJobInfoRequest(id)
	if err != nil {
		return err
	}

	time.Sleep(time.Second)
	scanResult, err := s.sendRetrieveImageRequest(id, token)
	if err != nil {
		return err
	}

	err = s.writeImageToFile(filename, scanResult.Image)
	if err != nil {
		return err
	}

	return nil
}

func (s Scanner) scanADF(directory string) error {
	if strings.Trim(directory, " ") == "" {
		log.Notice("directory not provided, generating")
		directory = s.generateDirectoryName()
	}

	log.Info("Scanning ADF into ", directory)

	r, err := s.sendScanRequest(InputSourceADF)
	if err != nil {
		return err
	}

	var id int = r.Body.CreateScanJobResponseType.JobId
	token := r.Body.CreateScanJobResponseType.JobToken

	err = s.prepareDirectory(directory)
	if err != nil {
		return err
	}
	i := 1
	for {
		log.Infof("Scanning page #%d", i)
		time.Sleep(time.Second)
		info, err := s.sendGetJobInfoRequest(id)
		if err != nil {
			return err
		}

		if info.Body.JobSummaryType.JobState != "Pending" && info.Body.JobSummaryType.JobState != "Processing" {
			log.Debugf("GetInfo returned %s, exiting", info.Body.JobSummaryType.JobState)
			break
		}

		time.Sleep(time.Second)
		scanResult, err := s.sendRetrieveImageRequest(id, token)
		if err != nil {
			_ = err.Error() // this is a hacky way to get the Subcode property set, the code setting it should be moved to another method, but it's 1 AM now
			if err.(*scannerError).Subcode == "wscn:ClientErrorNoImagesAvailable" {
				break
			}
			return err
		}

		filename := s.generateFileName(directory)
		err = s.writeImageToFile(filename, scanResult.Image)
		if err != nil {
			return err
		}
		log.Infof("Done with page #%d", i)
		i++
	}

	log.Info("Done ADF scanning")
	return nil
}

func (s Scanner) makeXMLRequest(requestStructure interface{}, responseStructure interface{}) (interface{}, error) {
	output, err := xml.MarshalIndent(requestStructure, "", "")
	if err != nil {
		return nil, err
	}

	bodyBytes, err := s.makeRequest(output)
	if err != nil {
		return nil, err
	}

	err = xml.Unmarshal(bodyBytes, &responseStructure)
	if err != nil {
		return nil, err
	}
	return responseStructure, err
}

func (s Scanner) makeRequest(request []byte) ([]byte, error) {
	log.Debugf("Making request to http://%s:%d, request body: %s\n", s.address, s.port, string(request))
	resp, err := http.Post(fmt.Sprintf("http://%s:%d", s.address, s.port), "text/xml; charset=utf-8", bytes.NewBuffer([]byte(request)))
	if err != nil {
		return nil, err
	}

	if resp.StatusCode >= 400 {
		return nil, &scannerError{resp: resp}
	}

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	return bodyBytes, nil
}
