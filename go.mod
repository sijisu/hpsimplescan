module gitlab.com/sijisu/hpsimplescan

go 1.15

require (
	github.com/google/uuid v1.2.0
	github.com/kaitai-io/kaitai_struct_go_runtime v0.0.0-20210123225611-a8be0c954b0e
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7
	github.com/urfave/cli/v2 v2.3.0
)
