package main

import "encoding/xml"

/*
====================================================
	envelope
====================================================
*/

type envelope struct {
	XMLName              xml.Name `xml:"SOAP-ENV:Envelope"`
	XMLnsxsd             string   `xml:"xmlns:xsd,attr"`
	XMLnsxsi             string   `xml:"xmlns:xsi,attr"`
	XMLnsSOAPENC         string   `xml:"xmlns:SOAP-ENC,attr"`
	SOAPENVencodingStyle string   `xml:"SOAP-ENV:encodingStyle,attr"`
	XMLnsSOAPENV         string   `xml:"xmlns:SOAP-ENV,attr"`
}

func (e *envelope) prepare() {
	e.XMLnsxsd = "http://www.w3.org/2001/XMLSchema"
	e.XMLnsxsi = "http://www.w3.org/2001/XMLSchema-instance"
	e.XMLnsSOAPENC = "http://www.w3.org/2003/05/soap-encoding"
	e.SOAPENVencodingStyle = "http://www.w3.org/2003/05/soap-encoding"
	e.XMLnsSOAPENV = "http://www.w3.org/2003/05/soap-envelope"
	return
}

/*
====================================================
	getScannerElements
====================================================
*/

type getScannerElementsRequest struct {
	envelope
	GetScannerElementsTag struct {
		XMLns string `xml:"xmlns,attr"`
	} `xml:"SOAP-ENV:Body>GetScannerElements"`
}

func (r *getScannerElementsRequest) prepare() {
	r.envelope.prepare()
	r.GetScannerElementsTag.XMLns = "http://tempuri.org/wscn.xsd"
	return
}

type getScannerElementsResponse struct {
	Body struct {
		ScanElements struct {
			ScannerConfiguration struct {
				DeviceSettings struct {
					FormatSupported struct {
						Type      string   `xml:"type,attr"`
						ItemType  string   `xml:"itemType,attr"`
						ArraySize string   `xml:"arraySize,attr"`
						Item      []string `xml:"item"`
					} `xml:"FormatSupported"`
					QualityFactorSupported string `xml:"QualityFactorSupported"`
					ContentSupported       struct {
						Type      string `xml:"type,attr"`
						ItemType  string `xml:"itemType,attr"`
						ArraySize string `xml:"arraySize,attr"`
						Item      string `xml:"item"`
					} `xml:"ContentSupported"`
					DocumentSizeAutoDetectSupported string `xml:"DocumentSizeAutoDetectSupported"`
					FeederCapacity                  string `xml:"FeederCapacity"`
					DuplexRotation                  string `xml:"DuplexRotation"`
					Rotation                        string `xml:"Rotation"`
				} `xml:"DeviceSettings"`
				Platen struct {
					ColorSupported struct {
						Type      string   `xml:"type,attr"`
						ItemType  string   `xml:"itemType,attr"`
						ArraySize string   `xml:"arraySize,attr"`
						Item      []string `xml:"item"`
					} `xml:"ColorSupported"`
					PlatenMinimumSize struct {
						Width  string `xml:"Width"`
						Height string `xml:"Height"`
					} `xml:"PlatenMinimumSize"`
					PlatenMaximumSize struct {
						Width  string `xml:"Width"`
						Height string `xml:"Height"`
					} `xml:"PlatenMaximumSize"`
					PlatenOpticalResolution struct {
						Width  string `xml:"Width"`
						Height string `xml:"Height"`
					} `xml:"PlatenOpticalResolution"`
					FlatbedSupported string `xml:"FlatbedSupported"`
				} `xml:"Platen"`
				ADF struct {
					ADFSupported      string `xml:"ADFSupported"`
					ADFSupportsDuplex string `xml:"ADFSupportsDuplex"`
					ADFMinimumSize    struct {
						Width  string `xml:"Width"`
						Height string `xml:"Height"`
					} `xml:"ADFMinimumSize"`
					ADFMaximumSize struct {
						Width  string `xml:"Width"`
						Height string `xml:"Height"`
					} `xml:"ADFMaximumSize"`
					ADFOpticalResolution struct {
						Width  string `xml:"Width"`
						Height string `xml:"Height"`
					} `xml:"ADFOpticalResolution"`
				} `xml:"ADF"`
			} `xml:"ScannerConfiguration"`
			ScannerStatus struct {
				ScannerCurrentTime string `xml:"ScannerCurrentTime"`
				ScannerState       string `xml:"ScannerState"`
				ScannerStateReason string `xml:"ScannerStateReason"`
				ScanToStatus       struct {
					PaperInADF      string `xml:"PaperInADF"`
					ScanToAvailable string `xml:"ScanToAvailable"`
				} `xml:"ScanToStatus"`
			} `xml:"ScannerStatus"`
			ScannerInfo struct {
				ModelNumber string `xml:"ModelNumber"`
			} `xml:"ScannerInfo"`
		} `xml:"ScanElements"`
	} `xml:"Body"`
}

/*
====================================================
	createScanJobRequest
====================================================
*/

type createScanJobRequestRequest struct {
	envelope
	Body struct {
		Text                 string `xml:",chardata"`
		CreateScanJobRequest struct {
			Text           string `xml:",chardata"`
			M              string `xml:"xmlns:m,attr"`
			ScanIdentifier struct {
				Text string `xml:",chardata"`
				Type string `xml:"xsi:type,attr"`
			} `xml:"ScanIdentifier"`
			ScanTicket struct {
				Text           string `xml:",chardata"`
				Xmlns          string `xml:"xmlns,attr"`
				Type           string `xml:"xsi:type,attr"`
				JobDescription struct {
					Text                   string `xml:",chardata"`
					Xmlns                  string `xml:"xmlns,attr"`
					Type                   string `xml:"xsi:type,attr"`
					JobOriginatingUserName struct {
						Text string `xml:",chardata"`
						Type string `xml:"xsi:type,attr"`
					} `xml:"JobOriginatingUserName"`
					JobName struct {
						Text string `xml:",chardata"`
						Type string `xml:"xsi:type,attr"`
					} `xml:"JobName"`
				} `xml:"JobDescription"`
				DocumentParameters struct {
					Text   string `xml:",chardata"`
					Xmlns  string `xml:"xmlns,attr"`
					Type   string `xml:"xsi:type,attr"`
					Format struct {
						Text  string `xml:",chardata"`
						Xmlns string `xml:"xmlns,attr"`
						Type  string `xml:"xsi:type,attr"`
					} `xml:"Format"`
					ImagesToTransfer struct {
						Text int    `xml:",chardata"`
						Type string `xml:"xsi:type,attr"`
					} `xml:"ImagesToTransfer"`
					Exposure struct {
						Text         string `xml:",chardata"`
						Xmlns        string `xml:"xmlns,attr"`
						Type         string `xml:"xsi:type,attr"`
						AutoExposure struct {
							Text bool   `xml:",chardata"`
							Type string `xml:"xsi:type,attr"`
						} `xml:"AutoExposure"`
						ExposureSettings struct {
							Text     string `xml:",chardata"`
							Xmlns    string `xml:"xmlns,attr"`
							Type     string `xml:"xsi:type,attr"`
							Contrast struct {
								Text int    `xml:",chardata"`
								Type string `xml:"xsi:type,attr"`
							} `xml:"Contrast"`
						} `xml:"ExposureSettings"`
					} `xml:"Exposure"`
					ContentType struct {
						Text string `xml:",chardata"`
						Type string `xml:"xsi:type,attr"`
					} `xml:"ContentType"`
					CompressionQualityFactor struct {
						Text int    `xml:",chardata"`
						Type string `xml:"xsi:type,attr"`
					} `xml:"CompressionQualityFactor"`
					InputSource struct {
						Text string `xml:",chardata"`
						Type string `xml:"xsi:type,attr"`
					} `xml:"InputSource"`
					InputSize struct {
						Text           string `xml:",chardata"`
						Xmlns          string `xml:"xmlns,attr"`
						Type           string `xml:"xsi:type,attr"`
						InputMediaSize struct {
							Text  string `xml:",chardata"`
							Xmlns string `xml:"xmlns,attr"`
							Type  string `xml:"xsi:type,attr"`
							Width struct {
								Text int    `xml:",chardata"`
								Type string `xml:"xsi:type,attr"`
							} `xml:"Width"`
							Height struct {
								Text int    `xml:",chardata"`
								Type string `xml:"xsi:type,attr"`
							} `xml:"Height"`
						} `xml:"InputMediaSize"`
					} `xml:"InputSize"`
					MediaSides struct {
						Text       string `xml:",chardata"`
						Xmlns      string `xml:"xmlns,attr"`
						Type       string `xml:"xsi:type,attr"`
						MediaFront struct {
							Text       string `xml:",chardata"`
							Xmlns      string `xml:"xmlns,attr"`
							Type       string `xml:"xsi:type,attr"`
							Resolution struct {
								Text  string `xml:",chardata"`
								Xmlns string `xml:"xmlns,attr"`
								Type  string `xml:"xsi:type,attr"`
								Width struct {
									Text int    `xml:",chardata"`
									Type string `xml:"xsi:type,attr"`
								} `xml:"Width"`
								Height struct {
									Text int    `xml:",chardata"`
									Type string `xml:"xsi:type,attr"`
								} `xml:"Height"`
							} `xml:"Resolution"`
							ScanRegion struct {
								Text              string `xml:",chardata"`
								Xmlns             string `xml:"xmlns,attr"`
								Type              string `xml:"xsi:type,attr"`
								ScanRegionXOffset struct {
									Text int    `xml:",chardata"`
									Type string `xml:"xsi:type,attr"`
								} `xml:"ScanRegionXOffset"`
								ScanRegionYOffset struct {
									Text int    `xml:",chardata"`
									Type string `xml:"xsi:type,attr"`
								} `xml:"ScanRegionYOffset"`
								ScanRegionHeight struct {
									Text int    `xml:",chardata"`
									Type string `xml:"xsi:type,attr"`
								} `xml:"ScanRegionHeight"`
								ScanRegionWidth struct {
									Text int    `xml:",chardata"`
									Type string `xml:"xsi:type,attr"`
								} `xml:"ScanRegionWidth"`
							} `xml:"ScanRegion"`
							ColorProcessing struct {
								Text string `xml:",chardata"`
								Type string `xml:"xsi:type,attr"`
							} `xml:"ColorProcessing"`
						} `xml:"MediaFront"`
					} `xml:"MediaSides"`
				} `xml:"DocumentParameters"`
				RetrieveImageTimeout struct {
					Text int    `xml:",chardata"`
					Type string `xml:"xsi:type,attr"`
				} `xml:"RetrieveImageTimeout"`
			} `xml:"ScanTicket"`
		} `xml:"m:CreateScanJobRequest"`
	} `xml:"SOAP-ENV:Body"`
}

func (r *createScanJobRequestRequest) prepare() {
	r.envelope.prepare()

	request := &r.Body.CreateScanJobRequest
	request.M = "http://tempuri.org/wscn.xsd"
	request.ScanIdentifier.Type = "xsd:string"

	ticket := &request.ScanTicket
	ticket.Type = "ScanTicketType"
	ticket.Xmlns = "http://tempuri.org/wscn.xsd"
	ticket.JobDescription.Xmlns = "http://tempuri.org/wscn.xsd"
	ticket.JobDescription.Type = "JobDescriptionType"
	ticket.JobDescription.JobOriginatingUserName.Type = "xsd:string"
	ticket.JobDescription.JobName.Type = "xsd:string"

	params := &ticket.DocumentParameters
	params.Xmlns = "http://tempuri.org/wscn.xsd"
	params.Type = "DocumentParametersType"
	params.Format.Xmlns = "http://tempuri.org/wscn.xsd"
	params.Format.Type = "ScanDocumentFormat"
	params.ImagesToTransfer.Text = 0
	params.Exposure.Xmlns = "http://tempuri.org/wscn.xsd"
	params.Exposure.Type = "ScanExposureType"
	params.Exposure.AutoExposure.Type = "xsd:boolean"
	params.Exposure.ExposureSettings.Xmlns = "http://tempuri.org/wscn.xsd"
	params.Exposure.ExposureSettings.Type = "ExposureSettingsOverrideType"
	params.Exposure.ExposureSettings.Contrast.Type = "xsd:int"
	params.ContentType.Type = "ContentType"
	params.CompressionQualityFactor.Type = "xsd:int"
	params.InputSource.Type = "ScanInputSource"
	params.InputSize.Xmlns = "http://tempuri.org/wscn.xsd"
	params.InputSize.Type = "DocumentInputSizeType"
	params.InputSize.InputMediaSize.Xmlns = "http://tempuri.org/wscn.xsd"
	params.InputSize.InputMediaSize.Type = "DimensionsType"
	params.InputSize.InputMediaSize.Width.Type = "xsd:int"
	params.InputSize.InputMediaSize.Height.Type = "xsd:int"
	params.MediaSides.Xmlns = "http://tempuri.org/wscn.xsd"
	params.MediaSides.Type = "MediaSideOverrideType"
	params.MediaSides.MediaFront.Xmlns = "http://tempuri.org/wscn.xsd"
	params.MediaSides.MediaFront.Type = "MediaSideOverrideType"
	params.MediaSides.MediaFront.Resolution.Xmlns = "http://tempuri.org/wscn.xsd"
	params.MediaSides.MediaFront.Resolution.Type = "MediaSideOverrideType"
	params.MediaSides.MediaFront.Resolution.Width.Type = "xsd:int"
	params.MediaSides.MediaFront.Resolution.Height.Type = "xsd:int"
	params.MediaSides.MediaFront.ScanRegion.Xmlns = "http://tempuri.org/wscn.xsd"
	params.MediaSides.MediaFront.ScanRegion.Type = "ScanRegionType"
	params.MediaSides.MediaFront.ScanRegion.ScanRegionHeight.Type = "xsd:int"
	params.MediaSides.MediaFront.ScanRegion.ScanRegionWidth.Type = "xsd:int"
	params.MediaSides.MediaFront.ScanRegion.ScanRegionXOffset.Type = "xsd:int"
	params.MediaSides.MediaFront.ScanRegion.ScanRegionYOffset.Type = "xsd:int"
	params.MediaSides.MediaFront.ColorProcessing.Type = "ColorEntryType"

	ticket.RetrieveImageTimeout.Type = "xsd:int"
	return
}

type createScanJobRequestResponse struct {
	Body struct {
		Text                      string `xml:",chardata"`
		CreateScanJobResponseType struct {
			Text             string `xml:",chardata"`
			JobId            int    `xml:"JobId"`
			JobToken         string `xml:"JobToken"`
			ImageInformation struct {
				Text                string `xml:",chardata"`
				MediaFrontImageInfo struct {
					Text          string `xml:",chardata"`
					PixelsPerLine string `xml:"PixelsPerLine"`
					NumberOfLines string `xml:"NumberOfLines"`
					BytesPerLine  string `xml:"BytesPerLine"`
				} `xml:"MediaFrontImageInfo"`
			} `xml:"ImageInformation"`
			DocumentFinalParameters struct {
				Text                     string `xml:",chardata"`
				Format                   string `xml:"Format"`
				CompressionQualityFactor string `xml:"CompressionQualityFactor"`
				ImagesToTransfer         string `xml:"ImagesToTransfer"`
				InputSource              string `xml:"InputSource"`
				ContentType              string `xml:"ContentType"`
				InputSize                struct {
					Text           string `xml:",chardata"`
					InputMediaSize struct {
						Text   string `xml:",chardata"`
						Width  string `xml:"Width"`
						Height string `xml:"Height"`
					} `xml:"InputMediaSize"`
					DocumentSizeAutoDetect string `xml:"DocumentSizeAutoDetect"`
				} `xml:"InputSize"`
				Exposure struct {
					Text             string `xml:",chardata"`
					AutoExposure     string `xml:"AutoExposure"`
					ExposureSettings struct {
						Text     string `xml:",chardata"`
						Contrast string `xml:"Contrast"`
					} `xml:"ExposureSettings"`
				} `xml:"Exposure"`
				MediaSides struct {
					Text       string `xml:",chardata"`
					MediaFront struct {
						Text       string `xml:",chardata"`
						ScanRegion struct {
							Text              string `xml:",chardata"`
							ScanRegionXOffset string `xml:"ScanRegionXOffset"`
							ScanRegionYOffset string `xml:"ScanRegionYOffset"`
							ScanRegionWidth   string `xml:"ScanRegionWidth"`
							ScanRegionHeight  string `xml:"ScanRegionHeight"`
						} `xml:"ScanRegion"`
						ColorProcessing string `xml:"ColorProcessing"`
						Resolution      struct {
							Text   string `xml:",chardata"`
							Width  string `xml:"Width"`
							Height string `xml:"Height"`
						} `xml:"Resolution"`
					} `xml:"MediaFront"`
				} `xml:"MediaSides"`
			} `xml:"DocumentFinalParameters"`
		} `xml:"CreateScanJobResponseType"`
	} `xml:"Body"`
}

/*
====================================================
	getJobInfo
====================================================
*/

type getJobInfoRequest struct {
	envelope
	Body struct {
		Text       string `xml:",chardata"`
		GetJobInfo struct {
			Text  string `xml:",chardata"`
			M     string `xml:"xmlns:m,attr"`
			JobId struct {
				Text int    `xml:",chardata"`
				Type string `xml:"xsi:type,attr"`
			} `xml:"jobId"`
		} `xml:"m:GetJobInfo"`
	} `xml:"SOAP-ENV:Body"`
}

func (r *getJobInfoRequest) prepare() {
	r.envelope.prepare()
	r.Body.GetJobInfo.M = "http://tempuri.org/wscn.xsd"
	r.Body.GetJobInfo.JobId.Type = "xsd:String"
	return
}

type getJobInfoResponse struct {
	Body struct {
		Text           string `xml:",chardata"`
		JobSummaryType struct {
			Text                   string `xml:",chardata"`
			JobId                  string `xml:"JobId"`
			JobName                string `xml:"JobName"`
			JobOriginatingUserName string `xml:"JobOriginatingUserName"`
			JobState               string `xml:"JobState"`
			JobStateReasons        string `xml:"JobStateReasons"`
			ScansCompleted         string `xml:"ScansCompleted"`
		} `xml:"JobSummaryType"`
	} `xml:"Body"`
}

/*
====================================================
	retrieveImageRequest
====================================================
*/

type retrieveImageRequestRequest struct {
	envelope
	Body struct {
		Text                 string `xml:",chardata"`
		RetrieveImageRequest struct {
			Text  string `xml:",chardata"`
			M     string `xml:"xmlns:m,attr"`
			JobId struct {
				Text int    `xml:",chardata"`
				Type string `xml:"xsi:type,attr"`
			} `xml:"JobId"`
			JobToken struct {
				Text string `xml:",chardata"`
				Type string `xml:"xsi:type,attr"`
			} `xml:"JobToken"`
		} `xml:"m:RetrieveImageRequest"`
	} `xml:"SOAP-ENV:Body"`
}

func (r *retrieveImageRequestRequest) prepare() {
	r.envelope.prepare()
	r.Body.RetrieveImageRequest.M = "http://tempuri.org/wscn.xsd"
	r.Body.RetrieveImageRequest.JobId.Type = "xsd:int"
	r.Body.RetrieveImageRequest.JobToken.Type = "xsd:string"
	return
}

type retrieveImageRequestResponse struct {
	Image []byte
	XML   struct {
		Body struct {
			Text                         string `xml:",chardata"`
			RetrieveImageRequestResponse struct {
				Text     string `xml:",chardata"`
				Response struct {
					Text string `xml:",chardata"`
					Href string `xml:"href,attr"`
				} `xml:"Response"`
			} `xml:"RetrieveImageRequestResponse"`
		} `xml:"Body"`
	}
}

/*
====================================================
	error
====================================================
*/

type errorResponse struct {
	Body struct {
		Fault struct {
			Code struct {
				Value   string `xml:"Value"`
				Subcode struct {
					Value string `xml:"Value"`
				} `xml:"Subcode"`
			} `xml:"Code"`
			Reason struct {
				Text string `xml:"Text"`
			} `xml:"Reason"`
		} `xml:"Fault"`
	} `xml:"Body"`
}
