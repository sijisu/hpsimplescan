package main

import (
	"encoding/json"
	"fmt"
	"os"

	"github.com/op/go-logging"
	"github.com/urfave/cli/v2"
)

// Logging
var log = logging.MustGetLogger("HPSimpleScan")
var format = logging.MustStringFormatter(`%{color}▶ %{level:.4s}%{color:reset}: %{message}`)

func main() {
	loggingBackend := logging.NewLogBackend(os.Stderr, "", 0)
	loggingFormatter := logging.NewBackendFormatter(loggingBackend, format)
	logging.SetBackend(loggingFormatter)

	cli.VersionFlag = &cli.BoolFlag{
		Name:    "version",
		Aliases: []string{"V"},
		Usage:   "print the version",
	}
	app := &cli.App{
		Name:    "HPSimpleScan",
		Version: "v0.2",
		Usage:   "simple scanning for some older HP printers/scanners, especially the HP LaserJet 100 colorMFP M175nw",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:  "i",
				Value: "192.168.1.3",
				Usage: "`IP` or hostname of the scanner/printer to connect to",
			},
			&cli.IntFlag{
				Name:  "p",
				Value: 8289,
				Usage: "`port` of the SOAP API on scanner/printer",
			},
			&cli.BoolFlag{
				Name:    "debug",
				Aliases: []string{"d"},
				Value:   false,
				Usage:   "debug output",
			},
			&cli.BoolFlag{
				Name:    "verbose",
				Aliases: []string{"v"},
				Value:   false,
				Usage:   "verbose output",
			},
		},
		Before: func(c *cli.Context) error {
			var level logging.Level
			if c.Bool("verbose") {
				level = logging.INFO
			} else if c.Bool("debug") {
				level = logging.DEBUG
			} else {
				level = logging.WARNING
			}

			logging.SetLevel(level, "")

			return nil
		},
		Commands: []*cli.Command{
			{
				Name:    "status",
				Aliases: []string{"i"},
				Usage:   "get the current scanner/printer status",
				Action: func(c *cli.Context) error {
					s := Scanner{address: c.String("i"), port: c.Int("p")}
					result, err := s.getStatus()
					if err != nil {
						log.Critical(err)
						return cli.Exit("", 1)
					}
					//Pretty print the result
					// TODO: make pretty table with the status, maybe using this https://github.com/olekukonko/tablewriter
					res, _ := json.MarshalIndent(result.Body.ScanElements, "", "\t")
					fmt.Printf("%s\n", string(res))
					return nil
				},
			},
			{
				Name:    "scan",
				Aliases: []string{"s"},
				Usage:   "scan from the scanner platen to file",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name: "f",
						//Aliases:     []string{"f"},
						Value:       "",
						DefaultText: "./scan-YYYYMMDD-HHMMSS.jpg",
						Usage:       "`file` where the scan should be saved",
					},
				},
				Action: func(c *cli.Context) error {
					s := Scanner{address: c.String("i"), port: c.Int("p")}
					err := s.scan(c.String("f"))
					if err != nil {
						log.Critical(err)
						return cli.Exit("", 1)
					}
					return nil
				},
			},
			{
				Name:    "scanadf",
				Aliases: []string{"sa"},
				Usage:   "scan from the scanner ADF to folder",
				Action: func(c *cli.Context) error {
					s := Scanner{address: c.String("i"), port: c.Int("p")}
					err := s.scanADF("") // there should be an option to specify folder to scan to
					if err != nil {
						log.Critical(err)
						return cli.Exit("", 1)
					}
					return nil
				},
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
